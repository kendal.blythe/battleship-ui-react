/**
 * This script merges the coverage reports from Jest and Cypress into a single one,
 * inside the "coverage/combined" folder.
 */
const { execSync } = require('child_process');
const fs = require('fs-extra');

const NYC_OUTPUT_FOLDER = '.nyc_output';
const REPORTS_FOLDER = 'coverage/reports';
const FINAL_OUTPUT_FOLDER = 'coverage/combined';

const run = commands => {
  commands.forEach(command => execSync(command, { stdio: 'inherit' }));
};

// Create the reports folder and move the reports from Jeset and Cypress inside it
fs.emptyDirSync(REPORTS_FOLDER);
fs.copyFileSync('coverage/cypress/coverage-final.json', `${REPORTS_FOLDER}/from-cypress.json`);
fs.copyFileSync('coverage/jest/coverage-final.json', `${REPORTS_FOLDER}/from-jest.json`);

fs.emptyDirSync(NYC_OUTPUT_FOLDER);
fs.emptyDirSync(FINAL_OUTPUT_FOLDER);

// Run "nyc merge" inside the reports folder, merging the two coverage files into one,
// then generate the final report on the coverage folder
run([
  // "nyc merge" will create a "coverage.json" file on the root, we move it to .nyc_output
  `nyc merge ${REPORTS_FOLDER} && mv coverage.json .nyc_output/out.json`,
  `nyc report --reporter html --reporter text --report-dir ${FINAL_OUTPUT_FOLDER}`
]);

fs.removeSync(NYC_OUTPUT_FOLDER);
fs.removeSync(REPORTS_FOLDER);
