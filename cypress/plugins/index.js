const tsPreprocessor = require('./cy-ts-preprocessor');
const codeCoverageTask = require('@cypress/code-coverage/task');

module.exports = (on, config) => {
  on('file:preprocessor', tsPreprocessor);
  codeCoverageTask(on, config);
  return config;
};
