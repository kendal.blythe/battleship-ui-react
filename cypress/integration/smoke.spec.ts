export {}; // make file a module without imports

it('should configure grid and play game', () => {
  // setup alias for api route
  cy.server();
  cy.route('POST', '**/battleship/graphql').as('api');

  // visit app
  cy.visit('/');
  cy.wait('@api');
  cy.wait('@api');

  // select 9x9 grid configuration
  cy.get('select')
    .should('have.value', 'GRID_10_10_5')
    .select('9x9 (4 ships)');
  cy.wait('@api');

  // select 6x6 grid configuration
  cy.get('select')
    .should('have.value', 'GRID_9_9_4')
    .select('6x6 (3 ships)');
  cy.wait('@api');
  cy.get('select').should('have.value', 'GRID_6_6_3');

  // click shuffle ships
  cy.get('button:contains("Shuffle Ships")').click();
  cy.wait('@api');

  // click start game
  cy.get('button:contains("Start Game")').click();
  cy.wait('@api');
  cy.get('button:contains("Quit Game")');

  // drop bombs until game over
  const coordinates = new Array(36); // 36 coordinates in 6x6 grid
  cy.wrap(coordinates).each(() => {
    cy.get('body').then(body => {
      if (body.find('button[data-row][data-col]').length > 0) {
        cy.get('button[data-row][data-col]')
          .first()
          .click();
        cy.wait('@api');
        cy.wait(100);
      }
    });
  });

  // assert game over
  cy.get('button:contains("Start Over")');
});
