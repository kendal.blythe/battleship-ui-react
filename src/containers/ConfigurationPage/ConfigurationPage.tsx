import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import {
  AppHeading,
  Button,
  ButtonDesign,
  MessageBox,
  OceanGrid,
  OceanGridDisplaySize,
  Select,
  SelectChangeEvent,
  Spinner,
  Toolbar,
  ToolbarSpacer
} from '../../components';
import { getLoading, getError } from '../../graphql';
import * as mutations from '../../graphql/mutations';
import { toGridInput } from '../../graphql/mapper';
import { Grid, GridConfig } from '../../graphql/battleship-api-types';
import classes from './ConfigurationPage.module.scss';

interface Props {
  gridConfigs: Array<GridConfig>;
  grid: Grid;
}

const ConfigurationPage: FC<Props> = ({ gridConfigs, grid }: Props) => {
  const { t } = useTranslation();

  const [createGrid, createGridMutation] = mutations.useCreateGrid();
  const [startGame, startGameMutation] = mutations.useStartGame();

  const onStartGame = (): void => {
    const gridInput = toGridInput(grid);
    startGame({ variables: { grid: gridInput } });
  };

  const onGridConfigChange = (gridConfigId: string): void => {
    createGrid({ variables: { gridConfigId } });
  };

  const onShuffleShips = (): void => {
    createGrid({ variables: { gridConfigId: grid.gridConfigId } });
  };

  const loading = getLoading(createGridMutation, startGameMutation);
  const error = getError(createGridMutation, startGameMutation);

  if (error) return <MessageBox text={t('App.errorMessage')} />;

  const options = gridConfigs.map(({ id, name }) => {
    return { value: id, label: name };
  });

  return (
    <div className={classes.ConfigurationPage} data-testid="configurationPage">
      <header>
        <Toolbar>
          <AppHeading text={t('ConfigurationPage.title')} />
          <Select
            testId="gridConfigSelect"
            value={grid.gridConfigId}
            options={options}
            onChange={(event: SelectChangeEvent): void =>
              onGridConfigChange(event.value.toString())
            }
          />
          <ToolbarSpacer />
          <Button text={t('ConfigurationPage.shuffleShipsButton')} onClick={onShuffleShips} />
          <Button
            text={t('ConfigurationPage.startGameButton')}
            design={ButtonDesign.Primary}
            onClick={onStartGame}
          />
        </Toolbar>
      </header>
      <main>
        <OceanGrid
          className={classes.oceanGrid}
          grid={grid}
          displaySize={OceanGridDisplaySize.Medium}
        />
      </main>
      {loading && <Spinner visibilityDelay={250} />}
    </div>
  );
};

export default ConfigurationPage;
