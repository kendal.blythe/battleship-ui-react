import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import { I18nextProvider } from 'react-i18next';
import i18n from '../../i18n/i18nForTest';
import { GraphQLError } from 'graphql';
import ConfigurationPage from './ConfigurationPage';
import * as mutations from '../../graphql/mutations';
import * as mocks from '../../graphql/mocks';
import { toGridInput } from '../../graphql/mapper';

const MOCKS = [
  {
    request: {
      query: mutations.START_GAME,
      variables: { grid: toGridInput(mocks.GRID) }
    },
    result: {
      data: {
        startGame: mocks.GAME
      }
    }
  },
  {
    request: {
      query: mutations.CREATE_GRID,
      variables: { gridConfigId: 'GRID_6_6_3' }
    },
    result: {
      data: {
        createGrid: mocks.GRID
      }
    }
  }
] as Array<MockedResponse>;

const ERROR_MOCKS = [
  {
    request: {
      query: mutations.START_GAME,
      variables: { grid: toGridInput(mocks.GRID) }
    },
    result: {
      errors: [new GraphQLError('Test Error')]
    }
  }
] as Array<MockedResponse>;

const assertResult = (result: RenderResult): void => {
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expect(component).toHaveClass('ConfigurationPage');
};

it('should render configuration page', () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <ConfigurationPage gridConfigs={mocks.GRID_CONFIGS} grid={mocks.GRID} />
      </I18nextProvider>
    </MockedProvider>
  );
  assertResult(result);
});

it('should start game on start game button click', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <ConfigurationPage gridConfigs={mocks.GRID_CONFIGS} grid={mocks.GRID} />
      </I18nextProvider>
    </MockedProvider>
  );
  assertResult(result);

  const button = result.queryByText(i18n.t('ConfigurationPage.startGameButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.START_GAME
  });
});

it('should create grid on grid config change', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <ConfigurationPage gridConfigs={mocks.GRID_CONFIGS} grid={mocks.GRID} />
      </I18nextProvider>
    </MockedProvider>
  );
  assertResult(result);

  const select = screen.queryByTestId('gridConfigSelect');
  expect(select).toBeInTheDocument();
  userEvent.selectOptions(select!, 'GRID_6_6_3');

  await waitFor(() => {
    // mutations.CREATE_GRID
  });
});

it('should create grid on shuffle ships button click', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <ConfigurationPage gridConfigs={mocks.GRID_CONFIGS} grid={mocks.GRID} />
      </I18nextProvider>
    </MockedProvider>
  );
  assertResult(result);

  const button = result.queryByText(i18n.t('ConfigurationPage.shuffleShipsButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.CREATE_GRID
  });
});

it('should return error on start game button click', async () => {
  process.env.REACT_APP_IGNORE_GRAPHQL_ERROR = '1';

  const result = render(
    <MockedProvider addTypename={true} mocks={ERROR_MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <ConfigurationPage gridConfigs={mocks.GRID_CONFIGS} grid={mocks.GRID} />
      </I18nextProvider>
    </MockedProvider>
  );
  assertResult(result);

  const button = result.queryByText(i18n.t('ConfigurationPage.startGameButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.START_GAME
  });

  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expect(component).toHaveClass('MessageBox');
  expect(result.queryByText(i18n.t('App.errorMessage'))).toBeInTheDocument();

  delete process.env.REACT_APP_IGNORE_GRAPHQL_ERROR;
});
