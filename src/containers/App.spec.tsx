import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { screen, waitFor } from '@testing-library/dom';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import { I18nextProvider } from 'react-i18next';
import i18n from '../i18n/i18nForTest';
import { GraphQLError } from 'graphql';
import App from './App';
import * as queries from '../graphql/queries';
import * as mutations from '../graphql/mutations';
import * as mocks from '../graphql/mocks';

const MOCKS = [
  {
    request: {
      query: queries.STARTUP
    },
    result: {
      data: {
        gridConfigs: mocks.GRID_CONFIGS,
        currentGame: null
      }
    }
  },
  {
    request: {
      query: mutations.CREATE_GRID,
      variables: { gridConfigId: 'GRID_10_10_5' }
    },
    result: {
      data: {
        createGrid: mocks.GRID
      }
    }
  }
] as Array<MockedResponse>;

const CONTINUE_GAME_MOCKS = [
  {
    request: {
      query: queries.STARTUP
    },
    result: {
      data: {
        gridConfigs: mocks.GRID_CONFIGS,
        currentGame: mocks.GAME
      }
    }
  }
] as Array<MockedResponse>;

const ERROR_MOCKS = [
  {
    request: {
      query: queries.STARTUP
    },
    result: {
      data: {
        gridConfigs: mocks.GRID_CONFIGS,
        currentGame: null
      }
    }
  },
  {
    request: {
      query: mutations.CREATE_GRID,
      variables: { gridConfigId: 'GRID_10_10_5' }
    },
    result: {
      errors: [new GraphQLError('Test Error')]
    }
  }
] as Array<MockedResponse>;

const assertResult = (result: RenderResult, pageTestId: string): void => {
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expect(component).toHaveClass('App');

  const page = screen.queryByTestId(pageTestId);
  expect(page).toBeInTheDocument();
};

it('should render app starting on configuration page', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <App />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result, 'configurationPage'));
});

it('should render app starting on game page', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={CONTINUE_GAME_MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <App />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result, 'gamePage'));
});

it('should return error on app startup', async () => {
  process.env.REACT_APP_IGNORE_GRAPHQL_ERROR = '1';

  const result = render(
    <MockedProvider addTypename={true} mocks={ERROR_MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <App />
      </I18nextProvider>
    </MockedProvider>
  );

  await waitFor(() => {
    const component = result.container.firstChild as Element;
    expect(component.nodeName).toMatch(/div/i);
    expect(component).toHaveClass('MessageBox');
    expect(result.queryByText(i18n.t('App.errorMessage'))).toBeInTheDocument();
  });
});
