import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { MessageBox, Spinner } from '../components';
import ConfigurationPage from './ConfigurationPage/ConfigurationPage';
import GamePage from './GamePage/GamePage';
import { getLoading, getError } from '../graphql';
import * as queries from '../graphql/queries';
import * as mutations from '../graphql/mutations';
import classes from './App.module.scss';

const App: FC = () => {
  const { t } = useTranslation();

  const client = useApolloClient();
  const game = queries.getCachedGame(useQuery(queries.CACHED_GAME));
  const grid = queries.getCachedGrid(useQuery(queries.CACHED_GRID));
  const [createGrid, createGridMutation] = mutations.useCreateGrid();

  const startupQuery = useQuery(queries.STARTUP, {
    onCompleted: data => {
      if (data.currentGame) {
        mutations.setCachedGame(data.currentGame, client);
      } else {
        createGrid({ variables: { gridConfigId: data.gridConfigs[0].id } });
      }
    }
  });

  const gridConfigs = startupQuery.data ? startupQuery.data.gridConfigs : null;

  const loading = getLoading(startupQuery, createGridMutation);
  const error = getError(startupQuery, createGridMutation);

  if (error) return <MessageBox text={t('App.errorMessage')} />;

  return (
    <div className={classes.App}>
      {!loading && !game && gridConfigs && grid && (
        <ConfigurationPage gridConfigs={gridConfigs} grid={grid} />
      )}
      {!loading && game && <GamePage game={game} />}
      {loading && <MessageBox text={t('App.loadingMessage')} />}
      {loading && <Spinner visibilityDelay={250} />}
    </div>
  );
};

export default App;
