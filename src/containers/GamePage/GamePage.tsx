import React, { FC, ReactElement, useRef, useLayoutEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/react-hooks';
import {
  AppHeading,
  Button,
  ButtonDesign,
  MessageBox,
  OceanGrid,
  OceanGridDisplaySize,
  Spinner,
  Toolbar,
  ToolbarSpacer
} from '../../components';
import { getLoading, getError } from '../../graphql';
import * as queries from '../../graphql/queries';
import * as mutations from '../../graphql/mutations';
import { Game, Grid } from '../../graphql/battleship-api-types';
import classes from './GamePage.module.scss';

interface Props {
  game: Game;
}

const GamePage: FC<Props> = ({ game }: Props) => {
  const { t } = useTranslation();

  // sync grid container widths with grid table width
  const tableRef = useRef<HTMLTableElement>(null);
  const yourContainerRef = useRef<HTMLDivElement>(null);
  const opponentContainerRef = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    const width = tableRef.current!.offsetWidth;
    if (width > 0) {
      yourContainerRef.current!.style.width = `${width}px`;
      opponentContainerRef.current!.style.width = `${width}px`;
    }
  }, [tableRef, yourContainerRef, opponentContainerRef]);

  const [endGame, endGameMutation] = mutations.useEndGame();

  const [createGrid, createGridMutation] = mutations.useCreateGrid(
    // post mutation function - call end game after create grid completed
    endGame.bind(undefined, {
      variables: {
        gameId: game.id,
        playerId: game.yourGrid.playerId
      }
    })
  );

  const [dropBomb, dropBombMutation] = mutations.useDropBomb();

  const onEndGame = (): void => {
    createGrid({ variables: { gridConfigId: game.yourGrid.gridConfigId } });
  };

  const onDropBomb = (x: number, y: number): void => {
    dropBomb({
      variables: {
        gameId: game.id,
        playerId: game.yourGrid.playerId,
        coordinate: { x, y }
      }
    });
  };

  const gameOver =
    game.winningPlayerNum === game.yourGrid.playerNum ||
    game.winningPlayerNum === game.opponentGrid.playerNum;

  const gameQuery = useQuery(queries.GAME, {
    variables: {
      gameId: game.id,
      playerId: game.yourGrid.playerId
    },
    skip: !gameOver || !!game.opponentGrid.ships
  });

  const displayGame = gameQuery.data?.game ?? game;

  const loading = getLoading(gameQuery, createGridMutation, dropBombMutation, endGameMutation);
  const error = getError(gameQuery, createGridMutation, dropBombMutation, endGameMutation);

  if (error) return <MessageBox text={t('App.errorMessage')} />;

  let displaySize;
  if (displayGame.yourGrid.size.x > 8) {
    displaySize = OceanGridDisplaySize.Small;
  } else if (displayGame.yourGrid.size.x < 7) {
    displaySize = OceanGridDisplaySize.Large;
  } else {
    displaySize = OceanGridDisplaySize.Medium;
  }

  const getSunkenShips = (grid: Grid): ReactElement | null => {
    const sunkenShipNames = grid.bombedCoordinates
      .filter(coordinate => !!coordinate.sunkenShip)
      .map(coordinate => coordinate.sunkenShip!.name);
    if (!sunkenShipNames.length) return null;
    const sunkenShips = t('GamePage.sunkenShipsText', {
      sunkenShipNames: sunkenShipNames.join(t('GamePage.sunkenShipDelimiter'))
    });
    return <span className={classes.sunkenShips}>{sunkenShips}</span>;
  };

  return (
    <div className={classes.GamePage} data-testid="gamePage">
      <header>
        <Toolbar>
          <AppHeading text={gameOver ? t('GamePage.gameOverTitle') : t('GamePage.title')} />
          <ToolbarSpacer />
          <Button
            text={gameOver ? t('GamePage.startOverButton') : t('GamePage.quitGameButton')}
            design={ButtonDesign.Primary}
            onClick={onEndGame}
          />
        </Toolbar>
      </header>
      <main>
        <div ref={yourContainerRef} className={classes.verticalLayout}>
          <h3>
            {t('GamePage.yourGridHeader')}
            {displayGame.winningPlayerNum === displayGame.yourGrid.playerNum &&
              t('GamePage.winnerGridHeader')}
          </h3>
          <OceanGrid tableRef={tableRef} grid={displayGame.yourGrid} displaySize={displaySize} />
          {getSunkenShips(displayGame.yourGrid)}
        </div>
        <div ref={opponentContainerRef} className={classes.verticalLayout}>
          <h3>
            {displayGame.opponentGrid.systemPlayer
              ? t('GamePage.computerGridHeader')
              : t('GamePage.opponentGridHeader')}
            {displayGame.winningPlayerNum === displayGame.opponentGrid.playerNum &&
              t('GamePage.winnerGridHeader')}
          </h3>
          {gameOver ? (
            <OceanGrid grid={displayGame.opponentGrid} displaySize={displaySize} />
          ) : (
            <OceanGrid
              grid={displayGame.opponentGrid}
              displaySize={displaySize}
              onDropBomb={onDropBomb}
            />
          )}
          {getSunkenShips(displayGame.opponentGrid)}
        </div>
      </main>
      {loading && <Spinner visibilityDelay={250} />}
    </div>
  );
};

export default GamePage;
