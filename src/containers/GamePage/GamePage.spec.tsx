import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import { I18nextProvider } from 'react-i18next';
import i18n from '../../i18n/i18nForTest';
import { GraphQLError } from 'graphql';
import GamePage from './GamePage';
import * as queries from '../../graphql/queries';
import * as mutations from '../../graphql/mutations';
import * as mocks from '../../graphql/mocks';

const MOCKS = [
  {
    request: {
      query: queries.GAME,
      variables: {
        gameId: mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN.id,
        playerId: mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN.yourGrid.playerId
      }
    },
    result: {
      data: {
        game: mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN
      }
    }
  },
  {
    request: {
      query: mutations.DROP_BOMB,
      variables: {
        gameId: mocks.GAME.id,
        playerId: mocks.GAME.yourGrid.playerId,
        coordinate: { x: 1, y: 1 }
      }
    },
    result: {
      data: {
        dropBomb: mocks.GAME
      }
    }
  },
  {
    request: {
      query: mutations.CREATE_GRID,
      variables: { gridConfigId: 'GRID_6_6_3' }
    },
    result: {
      data: {
        createGrid: mocks.GRID
      }
    }
  },
  {
    request: {
      query: mutations.END_GAME,
      variables: {
        gameId: mocks.GAME.id,
        playerId: mocks.GAME.yourGrid.playerId
      }
    },
    result: {
      data: {
        endGame: null
      }
    }
  }
] as Array<MockedResponse>;

const ERROR_MOCKS = [
  {
    request: {
      query: queries.GAME,
      variables: {
        gameId: mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN.id,
        playerId: mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN.yourGrid.playerId
      }
    },
    result: {
      data: {
        game: mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN
      }
    }
  },
  {
    request: {
      query: mutations.CREATE_GRID,
      variables: { gridConfigId: 'GRID_6_6_3' }
    },
    result: {
      errors: [new GraphQLError('Test Error')]
    }
  }
] as Array<MockedResponse>;

const assertResult = (result: RenderResult): void => {
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expect(component).toHaveClass('GamePage');
};

it('should render game page with small display size', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_SMALL_DISPLAY_SIZE} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));
});

it('should render game page with medium display size', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_MEDIUM_DISPLAY_SIZE} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));
});

it('should render game page with large display size', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_LARGE_DISPLAY_SIZE} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));
});

it('should drop bomb on coordinate click', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_NEW} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));

  const button = screen.queryByTestId(`buttonA1`);
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.DROP_BOMB
  });
});

it('should create grid and end game on quit game button click', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));

  const button = result.queryByText(i18n.t('GamePage.quitGameButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.CREATE_GRID
  });
  await waitFor(() => {
    // mutations.END_GAME
  });
});

it('should create grid and end game on start over button click on game over with opponent ships shown', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_OVER_OPPONENT_SHIPS_SHOWN} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));

  const button = result.queryByText(i18n.t('GamePage.startOverButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.CREATE_GRID
  });
  await waitFor(() => {
    // mutations.END_GAME
  });
});

it('should create grid and end game on start over button click 2 on game over with opponent ships hidden', async () => {
  const result = render(
    <MockedProvider addTypename={true} mocks={MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));

  const button = result.queryByText(i18n.t('GamePage.startOverButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.CREATE_GRID
  });
  await waitFor(() => {
    // mutations.END_GAME
  });
});

it('should return error on start over button click', async () => {
  process.env.REACT_APP_IGNORE_GRAPHQL_ERROR = '1';

  const result = render(
    <MockedProvider addTypename={true} mocks={ERROR_MOCKS} resolvers={{}}>
      <I18nextProvider i18n={i18n}>
        <GamePage game={mocks.GAME_OVER_OPPONENT_SHIPS_HIDDEN} />
      </I18nextProvider>
    </MockedProvider>
  );
  await waitFor(() => assertResult(result));

  const button = result.queryByText(i18n.t('GamePage.startOverButton'));
  expect(button).toBeInTheDocument();
  userEvent.click(button!);

  await waitFor(() => {
    // mutations.CREATE_GRID
  });

  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expect(component).toHaveClass('MessageBox');
  expect(result.queryByText(i18n.t('App.errorMessage'))).toBeInTheDocument();

  delete process.env.REACT_APP_IGNORE_GRAPHQL_ERROR;
});
