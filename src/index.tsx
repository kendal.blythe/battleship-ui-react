import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import App from './containers/App';
import { AppSuspense } from './components';
import './i18n/i18n';
import './index.scss';

if (process.env.NODE_ENV !== 'production') {
  require('whatwg-fetch'); // fetch polyfill for Cypress testing
}

const client = new ApolloClient({
  uri: process.env.REACT_APP_API_URL,
  credentials: 'include',
  resolvers: {}
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <AppSuspense>
      <App />
    </AppSuspense>
  </ApolloProvider>,
  document.getElementById('root')
);
