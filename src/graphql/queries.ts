import { gql } from 'apollo-boost';
import { QueryResult } from '@apollo/react-common';
import * as fragments from './fragments';
import { Game, Grid } from './battleship-api-types';

export const STARTUP = gql`
  {
    gridConfigs {
      id
      name
    }
    currentGame {
      ...GameFields
    }
  }
  ${fragments.GAME}
`;

export const GAME = gql`
  query getGame($gameId: String!, $playerId: String!) {
    game(gameId: $gameId, playerId: $playerId) {
      id
      yourGrid {
        gridConfigId
        size {
          x
          y
        }
        playerId
        playerNum
        ships {
          name
          length
          coordinates {
            x
            y
          }
        }
        bombedCoordinates {
          x
          y
          hit
          sunkenShip {
            name
          }
        }
      }
      opponentGrid {
        gridConfigId
        size {
          x
          y
        }
        playerNum
        systemPlayer
        ships {
          name
          length
          coordinates {
            x
            y
          }
        }
        bombedCoordinates {
          x
          y
          hit
          sunkenShip {
            name
          }
        }
      }
      winningPlayerNum
    }
  }
`;

export const CACHED_GAME = gql`
  {
    game @client {
      ...GameFields
    }
  }
  ${fragments.GAME}
`;

export const CACHED_GRID = gql`
  {
    grid @client {
      ...GridFields
    }
  }
  ${fragments.GRID}
`;

export const getCachedGame = (result: QueryResult): Game | null => {
  return result.data ? result.data.game : null;
};

export const getCachedGrid = (result: QueryResult): Grid | null => {
  return result.data ? result.data.grid : null;
};
