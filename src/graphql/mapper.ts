import {
  Coordinate,
  CoordinateInput,
  Grid,
  GridInput,
  GridSize,
  GridSizeInput,
  Ship,
  ShipInput
} from './battleship-api-types';

export const toCoordinateInput = (coordinate: Coordinate): CoordinateInput => {
  return {
    x: coordinate.x,
    y: coordinate.y
  };
};

export const toCoordinateInputs = (coordinates: Array<Coordinate>): Array<CoordinateInput> => {
  return coordinates.map(coordinate => toCoordinateInput(coordinate));
};

export const toShipInput = (ship: Ship): ShipInput => {
  return {
    name: ship.name,
    length: ship.length,
    coordinates: toCoordinateInputs(ship.coordinates)
  };
};

export const toShipInputs = (ships: Array<Ship>): Array<ShipInput> => {
  return ships.map(ship => toShipInput(ship));
};

export const toSizeInput = (size: GridSize): GridSizeInput => {
  return {
    x: size.x,
    y: size.y
  };
};

export const toGridInput = (grid: Grid): GridInput => {
  return {
    gridConfigId: grid.gridConfigId,
    size: toSizeInput(grid.size),
    ships: toShipInputs(grid.ships)
  };
};
