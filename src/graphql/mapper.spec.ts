import * as mapper from './mapper';
import {
  Coordinate,
  CoordinateInput,
  Grid,
  GridInput,
  GridSize,
  GridSizeInput,
  Ship,
  ShipInput
} from './battleship-api-types';

it('should return grid input for grid', () => {
  const grid = {
    gridConfigId: 'GRID_6_6_3',
    size: {
      x: 6,
      y: 6,
      __typename: 'GridSize'
    },
    ships: [
      {
        name: 'Battleship',
        length: 3,
        coordinates: [
          {
            x: 5,
            y: 1,
            __typename: 'Coordinate'
          },
          {
            x: 5,
            y: 2,
            __typename: 'Coordinate'
          },
          {
            x: 5,
            y: 3,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      },
      {
        name: 'Destroyer',
        length: 2,
        coordinates: [
          {
            x: 2,
            y: 2,
            __typename: 'Coordinate'
          },
          {
            x: 2,
            y: 3,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      },
      {
        name: 'Submarine',
        length: 1,
        coordinates: [
          {
            x: 3,
            y: 5,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      }
    ],
    __typename: 'Grid'
  } as Grid;

  const expectedGridInput = {
    gridConfigId: 'GRID_6_6_3',
    size: {
      x: 6,
      y: 6
    },
    ships: [
      {
        name: 'Battleship',
        length: 3,
        coordinates: [
          {
            x: 5,
            y: 1
          },
          {
            x: 5,
            y: 2
          },
          {
            x: 5,
            y: 3
          }
        ]
      },
      {
        name: 'Destroyer',
        length: 2,
        coordinates: [
          {
            x: 2,
            y: 2
          },
          {
            x: 2,
            y: 3
          }
        ]
      },
      {
        name: 'Submarine',
        length: 1,
        coordinates: [
          {
            x: 3,
            y: 5
          }
        ]
      }
    ]
  } as GridInput;

  expect(mapper.toGridInput(grid)).toMatchObject(expectedGridInput);
});

it('should return size input for size', () => {
  const size = {
    x: 6,
    y: 6,
    __typename: 'GridSize'
  } as GridSize;

  const expectedSize = {
    x: 6,
    y: 6
  } as GridSizeInput;

  expect(mapper.toSizeInput(size)).toMatchObject(expectedSize);
});

it('should return ship input for ship', () => {
  const ship = {
    name: 'Battleship',
    length: 3,
    coordinates: [
      {
        x: 5,
        y: 1,
        __typename: 'Coordinate'
      },
      {
        x: 5,
        y: 2,
        __typename: 'Coordinate'
      },
      {
        x: 5,
        y: 3,
        __typename: 'Coordinate'
      }
    ],
    __typename: 'Ship'
  } as Ship;

  const expectedShip = {
    name: 'Battleship',
    length: 3,
    coordinates: [
      {
        x: 5,
        y: 1
      },
      {
        x: 5,
        y: 2
      },
      {
        x: 5,
        y: 3
      }
    ]
  } as ShipInput;

  expect(mapper.toShipInput(ship)).toMatchObject(expectedShip);
});

it('should return ship inputs for ships', () => {
  const ships = [
    {
      name: 'Battleship',
      length: 3,
      coordinates: [
        {
          x: 5,
          y: 1,
          __typename: 'Coordinate'
        },
        {
          x: 5,
          y: 2,
          __typename: 'Coordinate'
        },
        {
          x: 5,
          y: 3,
          __typename: 'Coordinate'
        }
      ],
      __typename: 'Ship'
    },
    {
      name: 'Destroyer',
      length: 2,
      coordinates: [
        {
          x: 2,
          y: 2,
          __typename: 'Coordinate'
        },
        {
          x: 2,
          y: 3,
          __typename: 'Coordinate'
        }
      ],
      __typename: 'Ship'
    },
    {
      name: 'Submarine',
      length: 1,
      coordinates: [
        {
          x: 3,
          y: 5,
          __typename: 'Coordinate'
        }
      ],
      __typename: 'Ship'
    }
  ] as Array<Ship>;

  const expectedShips = [
    {
      name: 'Battleship',
      length: 3,
      coordinates: [
        {
          x: 5,
          y: 1
        },
        {
          x: 5,
          y: 2
        },
        {
          x: 5,
          y: 3
        }
      ]
    },
    {
      name: 'Destroyer',
      length: 2,
      coordinates: [
        {
          x: 2,
          y: 2
        },
        {
          x: 2,
          y: 3
        }
      ]
    },
    {
      name: 'Submarine',
      length: 1,
      coordinates: [
        {
          x: 3,
          y: 5
        }
      ]
    }
  ] as Array<ShipInput>;

  expect(mapper.toShipInputs(ships)).toMatchObject(expectedShips);
});

it('should return coordinate input for coordinate', () => {
  const coordinate = {
    x: 3,
    y: 5,
    __typename: 'Coordinate'
  } as Coordinate;

  const expectedCoordinate = {
    x: 3,
    y: 5
  } as CoordinateInput;

  expect(mapper.toCoordinateInput(coordinate)).toMatchObject(expectedCoordinate);
});

it('should return coordinate inputs for coordinates', () => {
  const coordinates = [
    {
      x: 5,
      y: 1,
      __typename: 'Coordinate'
    },
    {
      x: 5,
      y: 2,
      __typename: 'Coordinate'
    },
    {
      x: 5,
      y: 3,
      __typename: 'Coordinate'
    }
  ] as Array<Coordinate>;

  const expectedCoordinates = [
    {
      x: 5,
      y: 1
    },
    {
      x: 5,
      y: 2
    },
    {
      x: 5,
      y: 3
    }
  ] as Array<CoordinateInput>;

  expect(mapper.toCoordinateInputs(coordinates)).toMatchObject(expectedCoordinates);
});
