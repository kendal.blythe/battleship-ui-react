export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Query = {
  __typename?: 'Query';
  gridConfigs: Array<GridConfig>;
  gridConfig: GridConfig;
  game: Game;
  currentGame?: Maybe<Game>;
};

export type QueryGridConfigArgs = {
  gridConfigId: Scalars['String'];
};

export type QueryGameArgs = {
  gameId: Scalars['String'];
  playerId: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createGrid: Grid;
  startGame: Game;
  dropBomb: Game;
  endGame: Scalars['Boolean'];
};

export type MutationCreateGridArgs = {
  gridConfigId: Scalars['String'];
};

export type MutationStartGameArgs = {
  grid: GridInput;
};

export type MutationDropBombArgs = {
  gameId: Scalars['String'];
  playerId: Scalars['String'];
  coordinate: CoordinateInput;
};

export type MutationEndGameArgs = {
  gameId: Scalars['String'];
  playerId: Scalars['String'];
};

export type GridSize = {
  __typename?: 'GridSize';
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type GridSizeInput = {
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type Coordinate = {
  __typename?: 'Coordinate';
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type CoordinateInput = {
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type Ship = {
  __typename?: 'Ship';
  name: Scalars['String'];
  length: Scalars['Int'];
  coordinates: Array<Coordinate>;
};

export type ShipInput = {
  name: Scalars['String'];
  length: Scalars['Int'];
  coordinates: Array<CoordinateInput>;
};

export type BombedCoordinate = {
  __typename?: 'BombedCoordinate';
  x: Scalars['Int'];
  y: Scalars['Int'];
  hit: Scalars['Boolean'];
  sunkenShip?: Maybe<Ship>;
};

export type GridConfig = {
  __typename?: 'GridConfig';
  id: Scalars['String'];
  name: Scalars['String'];
  size: GridSize;
  ships: Array<Ship>;
};

export type Grid = {
  __typename?: 'Grid';
  gridConfigId: Scalars['String'];
  size: GridSize;
  ships: Array<Ship>;
  playerId: Scalars['String'];
  playerNum: Scalars['Int'];
  systemPlayer: Scalars['Boolean'];
  bombedCoordinates: Array<BombedCoordinate>;
};

export type GridInput = {
  gridConfigId: Scalars['String'];
  size: GridSizeInput;
  ships: Array<ShipInput>;
};

export type Game = {
  __typename?: 'Game';
  id: Scalars['String'];
  yourGrid: Grid;
  opponentGrid: Grid;
  winningPlayerNum?: Maybe<Scalars['Int']>;
};
