import { MutationResult, QueryResult } from '@apollo/react-common';
import { ApolloError } from 'apollo-boost';
import { getLoading, getError } from '.';

const createQueryResult = (loading: boolean): QueryResult => {
  return { loading } as QueryResult;
};

const createMutationResult = (error: boolean): MutationResult => {
  const apolloError = {
    message: 'Error'
  } as ApolloError;
  return { error: error ? apolloError : null } as MutationResult;
};

it('should return not loading', () => {
  const loading = getLoading(createQueryResult(false), createQueryResult(false));
  expect(loading).toBe(false);
});

it('should return loading', () => {
  const loading = getLoading(createQueryResult(false), createQueryResult(true));
  expect(loading).toBe(true);
});

it('should return no error', () => {
  const error = getError(createMutationResult(false), createMutationResult(false));
  expect(error).toBe(false);
});

it('should return error', () => {
  const error = getError(createMutationResult(false), createMutationResult(true));
  expect(error).toBe(true);
});
