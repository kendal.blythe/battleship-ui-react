import ApolloClient from 'apollo-boost';
import * as mutations from './mutations';
import { GAME, GRID } from './mocks';

const client = new ApolloClient<object>();

it('should set cached game', () => {
  mutations.setCachedGame(GAME, client);
});

it('should clear cached game', () => {
  mutations.setCachedGame(null, client);
});

it('should set cached grid', () => {
  mutations.setCachedGrid(GRID, client);
});

it('should clear cached grid', () => {
  mutations.setCachedGrid(null, client);
});
