import { MutationResult, QueryResult } from '@apollo/react-common';

type Result = MutationResult | QueryResult;

const loadingReducer = (loading: boolean, result: Result): boolean => {
  return loading || !!result.loading;
};

const errorReducer = (error: boolean, result: Result): boolean => {
  return error || !!result.error;
};

export const getLoading = (...operations: Array<Result>): boolean => {
  return operations.reduce(loadingReducer, false);
};

export const getError = (...operations: Array<Result>): boolean => {
  return operations.reduce(errorReducer, false);
};
