import { QueryResult } from '@apollo/react-common';
import * as queries from './queries';
import { GAME, GRID } from './mocks';

it('should return cached game', () => {
  const result = { data: { game: GAME } } as QueryResult;
  const game = queries.getCachedGame(result);
  expect(game).toBeTruthy();
});

it('should return null cached game', () => {
  let result = { data: { game: null } } as QueryResult;
  let game = queries.getCachedGame(result);
  expect(game).toBeFalsy();

  result = { data: null } as QueryResult;
  game = queries.getCachedGame(result);
  expect(game).toBeFalsy();
});

it('shoulde return cached grid', () => {
  const result = { data: { grid: GRID } } as QueryResult;
  const grid = queries.getCachedGrid(result);
  expect(grid).toBeTruthy();
});

it('should return null cached grid', () => {
  let result = { data: { grid: null } } as QueryResult;
  let grid = queries.getCachedGrid(result);
  expect(grid).toBeFalsy();

  result = { data: null } as QueryResult;
  grid = queries.getCachedGrid(result);
  expect(grid).toBeFalsy();
});
