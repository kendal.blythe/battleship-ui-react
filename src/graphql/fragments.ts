import { gql } from 'apollo-boost';

export const GAME = gql`
  fragment GameFields on Game {
    id
    yourGrid {
      gridConfigId
      size {
        x
        y
      }
      playerId
      playerNum
      ships {
        name
        length
        coordinates {
          x
          y
        }
      }
      bombedCoordinates {
        x
        y
        hit
        sunkenShip {
          name
        }
      }
    }
    opponentGrid {
      gridConfigId
      size {
        x
        y
      }
      playerNum
      systemPlayer
      bombedCoordinates {
        x
        y
        hit
        sunkenShip {
          name
        }
      }
    }
    winningPlayerNum
  }
`;

export const GRID = gql`
  fragment GridFields on Grid {
    gridConfigId
    size {
      x
      y
    }
    ships {
      name
      length
      coordinates {
        x
        y
      }
    }
  }
`;
