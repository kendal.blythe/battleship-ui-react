import ApolloClient, { gql } from 'apollo-boost';
import { MutationTuple, useApolloClient, useMutation } from '@apollo/react-hooks';
import * as fragments from './fragments';
import * as queries from './queries';
import { Game, Grid } from './battleship-api-types';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type MutationTupleAny = MutationTuple<any, any>;

export const CREATE_GRID = gql`
  mutation($gridConfigId: String!) {
    createGrid(gridConfigId: $gridConfigId) {
      ...GridFields
    }
  }
  ${fragments.GRID}
`;

export const START_GAME = gql`
  mutation($grid: GridInput!) {
    startGame(grid: $grid) {
      ...GameFields
    }
  }
  ${fragments.GAME}
`;

export const DROP_BOMB = gql`
  mutation($gameId: String!, $playerId: String!, $coordinate: CoordinateInput!) {
    dropBomb(gameId: $gameId, playerId: $playerId, coordinate: $coordinate) {
      ...GameFields
    }
  }
  ${fragments.GAME}
`;

export const END_GAME = gql`
  mutation($gameId: String!, $playerId: String!) {
    endGame(gameId: $gameId, playerId: $playerId)
  }
`;

interface IgnoreGraphqlErrorFunction {
  (): void;
}

const getOnError = (
  ignoreGraphqlError: string | undefined
): IgnoreGraphqlErrorFunction | undefined => {
  return ignoreGraphqlError
    ? (): void => {
        // ignore GraphQL error
      }
    : undefined;
};

export const setCachedGame = (game: Game | null, client: ApolloClient<object>): void => {
  client.writeQuery({
    query: queries.CACHED_GAME,
    data: { game }
  });
};

export const setCachedGrid = (grid: Grid | null, client: ApolloClient<object>): void => {
  client.writeQuery({
    query: queries.CACHED_GRID,
    data: { grid }
  });
};

export const useCreateGrid = (postMutationFunc?: () => void): MutationTupleAny => {
  const client = useApolloClient();
  return useMutation(CREATE_GRID, {
    onCompleted: data => {
      setCachedGrid(data.createGrid, client);
      postMutationFunc?.();
    },
    onError: getOnError(process.env.REACT_APP_IGNORE_GRAPHQL_ERROR)
  });
};

export const useStartGame = (): MutationTupleAny => {
  const client = useApolloClient();
  return useMutation(START_GAME, {
    onCompleted: data => {
      setCachedGame(data.startGame, client);
    },
    onError: getOnError(process.env.REACT_APP_IGNORE_GRAPHQL_ERROR)
  });
};

export const useDropBomb = (): MutationTupleAny => {
  const client = useApolloClient();
  return useMutation(DROP_BOMB, {
    onCompleted: data => {
      setCachedGame(data.dropBomb, client);
    },
    onError: getOnError(process.env.REACT_APP_IGNORE_GRAPHQL_ERROR)
  });
};

export const useEndGame = (): MutationTupleAny => {
  const client = useApolloClient();
  return useMutation(END_GAME, {
    onCompleted: () => {
      setCachedGame(null, client);
    },
    onError: getOnError(process.env.REACT_APP_IGNORE_GRAPHQL_ERROR)
  });
};
