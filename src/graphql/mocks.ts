import { Game, Grid, GridConfig } from './battleship-api-types';

export const GRID_CONFIGS = [
  {
    id: 'GRID_10_10_5',
    name: '10x10 (5 ships)',
    __typename: 'GridConfig'
  },
  {
    id: 'GRID_9_9_4',
    name: '9x9 (4 ships)',
    __typename: 'GridConfig'
  },
  {
    id: 'GRID_6_6_3',
    name: '6x6 (3 ships)',
    __typename: 'GridConfig'
  }
] as Array<GridConfig>;

export const GRID = {
  gridConfigId: 'GRID_6_6_3',
  size: {
    x: 6,
    y: 6,
    __typename: 'GridSize'
  },
  ships: [
    {
      name: 'Battleship',
      length: 3,
      coordinates: [
        {
          x: 1,
          y: 6,
          __typename: 'Coordinate'
        },
        {
          x: 2,
          y: 6,
          __typename: 'Coordinate'
        },
        {
          x: 3,
          y: 6,
          __typename: 'Coordinate'
        }
      ],
      __typename: 'Ship'
    },
    {
      name: 'Destroyer',
      length: 2,
      coordinates: [
        {
          x: 3,
          y: 3,
          __typename: 'Coordinate'
        },
        {
          x: 3,
          y: 4,
          __typename: 'Coordinate'
        }
      ],
      __typename: 'Ship'
    },
    {
      name: 'Submarine',
      length: 1,
      coordinates: [
        {
          x: 4,
          y: 6,
          __typename: 'Coordinate'
        }
      ],
      __typename: 'Ship'
    }
  ],
  __typename: 'Grid'
} as Grid;

export const GAME_OVER_OPPONENT_SHIPS_SHOWN = {
  id: '205de86b-0bfb-4441-9e94-374024ef07f7',
  yourGrid: {
    gridConfigId: 'GRID_6_6_3',
    size: {
      x: 6,
      y: 6,
      __typename: 'GridSize'
    },
    playerId: '466f5118-7010-4fba-9a14-29c1f0dba3e0',
    playerNum: 1,
    ships: [
      {
        name: 'Battleship',
        length: 3,
        coordinates: [
          {
            x: 2,
            y: 3,
            __typename: 'Coordinate'
          },
          {
            x: 2,
            y: 4,
            __typename: 'Coordinate'
          },
          {
            x: 2,
            y: 5,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      },
      {
        name: 'Destroyer',
        length: 2,
        coordinates: [
          {
            x: 4,
            y: 2,
            __typename: 'Coordinate'
          },
          {
            x: 4,
            y: 3,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      },
      {
        name: 'Submarine',
        length: 1,
        coordinates: [
          {
            x: 2,
            y: 1,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      }
    ],
    bombedCoordinates: [
      {
        x: 3,
        y: 3,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 3,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 3,
        hit: true,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 2,
        hit: true,
        sunkenShip: {
          name: 'Destroyer',
          __typename: 'Ship'
        },
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 1,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 1,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 4,
        hit: true,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 3,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 1,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 3,
        hit: true,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 5,
        hit: true,
        sunkenShip: {
          name: 'Battleship',
          __typename: 'Ship'
        },
        __typename: 'BombedCoordinate'
      },
      {
        x: 3,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 1,
        y: 1,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 3,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 2,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 1,
        y: 2,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      }
    ],
    __typename: 'Grid'
  },
  opponentGrid: {
    gridConfigId: 'GRID_6_6_3',
    size: {
      x: 6,
      y: 6,
      __typename: 'GridSize'
    },
    playerNum: 2,
    systemPlayer: true,
    ships: [
      {
        name: 'Battleship',
        length: 3,
        coordinates: [
          {
            x: 2,
            y: 4,
            __typename: 'Coordinate'
          },
          {
            x: 2,
            y: 5,
            __typename: 'Coordinate'
          },
          {
            x: 2,
            y: 6,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      },
      {
        name: 'Destroyer',
        length: 2,
        coordinates: [
          {
            x: 4,
            y: 1,
            __typename: 'Coordinate'
          },
          {
            x: 5,
            y: 1,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      },
      {
        name: 'Submarine',
        length: 1,
        coordinates: [
          {
            x: 5,
            y: 3,
            __typename: 'Coordinate'
          }
        ],
        __typename: 'Ship'
      }
    ],
    bombedCoordinates: [
      {
        x: 2,
        y: 2,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 3,
        y: 3,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 1,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 5,
        hit: true,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 3,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 6,
        hit: true,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 2,
        y: 4,
        hit: true,
        sunkenShip: {
          name: 'Battleship',
          __typename: 'Ship'
        },
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 3,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 2,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 1,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 3,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 1,
        hit: true,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 3,
        y: 1,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 1,
        hit: true,
        sunkenShip: {
          name: 'Destroyer',
          __typename: 'Ship'
        },
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 4,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 6,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 6,
        y: 5,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 4,
        hit: false,
        sunkenShip: null,
        __typename: 'BombedCoordinate'
      },
      {
        x: 5,
        y: 3,
        hit: true,
        sunkenShip: {
          name: 'Submarine',
          __typename: 'Ship'
        },
        __typename: 'BombedCoordinate'
      }
    ],
    __typename: 'Grid'
  },
  winningPlayerNum: 1,
  __typename: 'Game'
} as Game;

export const GAME_OVER_OPPONENT_SHIPS_HIDDEN = {
  ...GAME_OVER_OPPONENT_SHIPS_SHOWN,
  opponentGrid: {
    ...GAME_OVER_OPPONENT_SHIPS_SHOWN.opponentGrid,
    ships: []
  },
  winningPlayerNum: 2
};

export const GAME = {
  ...GAME_OVER_OPPONENT_SHIPS_HIDDEN,
  winningPlayerNum: null
};

export const GAME_NEW = {
  ...GAME,
  yourGrid: {
    ...GAME.yourGrid,
    bombedCoordinates: []
  },
  opponentGrid: {
    ...GAME.opponentGrid,
    bombedCoordinates: [],
    systemPlayer: false
  },
  winningPlayerNum: null
};

const getCustomGameSize = (x: number, y: number): Game => {
  return {
    ...GAME,
    yourGrid: {
      ...GAME.yourGrid,
      size: {
        ...GAME.yourGrid.size,
        x,
        y
      }
    },
    opponentGrid: {
      ...GAME.opponentGrid,
      size: {
        ...GAME.yourGrid.size,
        x,
        y
      }
    }
  };
};

export const GAME_SMALL_DISPLAY_SIZE = getCustomGameSize(10, 10);
export const GAME_MEDIUM_DISPLAY_SIZE = getCustomGameSize(8, 8);
export const GAME_LARGE_DISPLAY_SIZE = GAME;
