import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Backdrop } from '..';
import cx from 'classnames';

const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, className?: string): void => {
  const expectedClassNames = cx('Backdrop', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
};

it('should render visible backdrop', () => {
  const result = render(<Backdrop />);
  assertResult(result);
});

it('should render visible backdrop with custom class name', () => {
  const result = render(<Backdrop className={CUSTOM_CLASS_NAME} />);
  assertResult(result, CUSTOM_CLASS_NAME);
});

it('should render invisible backdrop', () => {
  const result = render(<Backdrop visible={false} />);
  assertResult(result, 'hidden');
});

it('should call click handler on click', () => {
  const onClick = jest.fn(() => undefined);

  const result = render(<Backdrop onClick={onClick} />);
  assertResult(result);

  const backdrop = result.container.firstChild as Element;
  userEvent.click(backdrop);
  expect(onClick).toHaveBeenCalledTimes(1);
});
