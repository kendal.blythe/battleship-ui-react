import React, { FC, MouseEvent } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './Backdrop.module.scss';

export interface BackdropProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Specifies whether or not the component is visible.
   */
  visible?: boolean;
  /**
   * Click event handler.
   */
  onClick?: (event: MouseEvent<HTMLElement>) => void;
}

/**
 * Displays a visible or invisible overlay over the entire screen to prevent user interactions
 * behind the overlay.
 */
export const Backdrop: FC<BackdropProps> = ({
  id,
  style,
  className,
  visible = true,
  onClick
}: BackdropProps) => (
  <div
    id={id}
    style={style}
    className={cx(classes.Backdrop, !visible && classes.hidden, className)}
    onClick={(event: MouseEvent<HTMLElement>): void => onClick?.(event)}
  />
);

export default Backdrop;
