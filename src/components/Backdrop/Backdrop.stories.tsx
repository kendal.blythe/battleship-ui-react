import React, { FC } from 'react';
import { action } from '@storybook/addon-actions';
import { Backdrop } from '..';

export default {
  title: 'Backdrop',
  component: Backdrop
};

export const BackdropVisible: FC = () => {
  return <Backdrop onClick={action('click')} />;
};

export const BackdropInvisible: FC = () => {
  return <Backdrop visible={false} onClick={action('click')} />;
};
