import React, { FC } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './AppHeading.module.scss';

export interface AppHeadingProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Heading text.
   */
  text: string;
}

/**
 * Displays an application heading.
 */
export const AppHeading: FC<AppHeadingProps> = ({
  id,
  style,
  className,
  text
}: AppHeadingProps) => (
  <h2 id={id} style={style} className={cx(classes.AppHeading, className)}>
    {text}
  </h2>
);

export default AppHeading;
