import React, { FC } from 'react';
import { text } from '@storybook/addon-knobs';
import { AppHeading } from '..';

export default {
  title: 'AppHeading',
  component: AppHeading
};

export const AppHeadingWithText: FC = () => {
  const label = text('Text:', '');
  return <AppHeading text={label || 'Battleship'} />;
};
