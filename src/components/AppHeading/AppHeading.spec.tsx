import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { AppHeading } from '..';
import cx from 'classnames';

const HEADING_TEXT = 'The Application Heading Text';
const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, className?: string): void => {
  const expectedClassNames = cx('AppHeading', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/h[1-5]/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
  expect(result.queryByText(HEADING_TEXT)).toBeInTheDocument();
};

it('should render app heading text', () => {
  const result = render(<AppHeading text={HEADING_TEXT} />);
  assertResult(result);
});

it('should render app heading text with custom class name', () => {
  const result = render(<AppHeading text={HEADING_TEXT} className={CUSTOM_CLASS_NAME} />);
  assertResult(result, CUSTOM_CLASS_NAME);
});
