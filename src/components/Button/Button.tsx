import React, { FC, MouseEvent } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './Button.module.scss';

export enum ButtonDesign {
  Primary = 'primary'
}

export interface ButtonProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Button text.
   */
  text: string;
  /**
   * Button design.
   */
  design?: ButtonDesign;
  /**
   * Click event handler.
   */
  onClick?: (event: MouseEvent<HTMLElement>) => void;
}

/**
 * Displays a button allowing the user to take action when the button is clicked.
 */
export const Button: FC<ButtonProps> = ({
  id,
  style,
  className,
  text,
  design,
  onClick
}: ButtonProps) => (
  <button
    id={id}
    style={style}
    className={cx(classes.Button, !!design && classes[design], className)}
    type="button"
    onClick={(event: MouseEvent<HTMLElement>): void => onClick?.(event)}
  >
    {text}
  </button>
);

export default Button;
