import React, { FC } from 'react';
import { text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { Button, ButtonDesign } from '..';

export default {
  title: 'Button',
  component: Button
};

export const ButtonWithNoDesign: FC = () => {
  const label = text('Text:', '');
  return <Button text={label || 'Default'} onClick={action('click')} />;
};

export const ButtonWithPrimaryDesign: FC = () => {
  const label = text('Text:', '');
  return (
    <Button text={label || 'Primary'} design={ButtonDesign.Primary} onClick={action('click')} />
  );
};
