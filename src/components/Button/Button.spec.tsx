import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Button, ButtonDesign } from '..';
import cx from 'classnames';

const BUTTON_TEXT = 'The Application Heading Text';
const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, className?: string): void => {
  const expectedClassNames = cx('Button', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/button/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
  expect(result.queryByText(BUTTON_TEXT)).toBeInTheDocument();
};

it('should render button', () => {
  const result = render(<Button text={BUTTON_TEXT} />);
  assertResult(result);
});

it('should render button with custom class name', () => {
  const result = render(<Button text={BUTTON_TEXT} className={CUSTOM_CLASS_NAME} />);
  assertResult(result, CUSTOM_CLASS_NAME);
});

it('should render button with primary design', () => {
  const result = render(<Button text={BUTTON_TEXT} design={ButtonDesign.Primary} />);
  assertResult(result, 'primary');
});

it('should call click handler on click', () => {
  const onClick = jest.fn(() => undefined);

  const result = render(<Button text={BUTTON_TEXT} onClick={onClick} />);
  assertResult(result);

  const button = result.container.firstChild as Element;
  userEvent.click(button);
  expect(onClick).toHaveBeenCalledTimes(1);
});
