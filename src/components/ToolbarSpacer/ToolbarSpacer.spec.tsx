import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { ToolbarSpacer } from '..';
import cx from 'classnames';

const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, className?: string): void => {
  const expectedClassNames = cx('ToolbarSpacer', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
};

it('should render toolbar spacer', () => {
  const result = render(<ToolbarSpacer />);
  assertResult(result);
});

it('should render toolbar spacer with custom class name', () => {
  const result = render(<ToolbarSpacer className={CUSTOM_CLASS_NAME} />);
  assertResult(result, CUSTOM_CLASS_NAME);
});
