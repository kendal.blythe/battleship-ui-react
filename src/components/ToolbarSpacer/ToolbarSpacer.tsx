import React, { FC } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './ToolbarSpacer.module.scss';

export interface ToolbarSpacerProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
}

/**
 * Displays space between toolbar elements.
 */
export const ToolbarSpacer: FC<ToolbarSpacerProps> = ({
  id,
  style,
  className
}: ToolbarSpacerProps) => (
  <div id={id} style={style} className={cx(classes.ToolbarSpacer, className)} />
);

export default ToolbarSpacer;
