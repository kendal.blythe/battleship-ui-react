import React, { FC, ReactElement, Suspense } from 'react';
import { Spinner } from '..';

export interface AppSuspenseProps {
  /**
   * Child elements.
   */
  children?: ReactElement | Array<ReactElement>;
}

/**
 * Application suspense.
 */
export const AppSuspense: FC<AppSuspenseProps> = ({ children }: AppSuspenseProps) => (
  <Suspense
    fallback={
      <Suspense fallback={<div style={{ display: 'none' }} />}>
        <Spinner />
      </Suspense>
    }
  >
    {children}
  </Suspense>
);

export default AppSuspense;
