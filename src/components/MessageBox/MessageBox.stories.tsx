import React, { FC } from 'react';
import { text } from '@storybook/addon-knobs';
import { MessageBox } from '..';

export default {
  title: 'MessageBox',
  component: MessageBox
};

export const MessageBoxWithText: FC = () => {
  const message = text('Text:', '');
  return (
    <MessageBox
      text={
        message || 'An error occurred contacting the Battleship server. Try reloading the page.'
      }
    />
  );
};
