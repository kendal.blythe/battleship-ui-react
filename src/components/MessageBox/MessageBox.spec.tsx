import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { MessageBox } from '..';
import cx from 'classnames';

const MESSAGE_TEXT = 'The message box text.';
const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, className?: string): void => {
  const expectedClassNames = cx('MessageBox', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
  expect(result.queryByText(MESSAGE_TEXT)).toBeInTheDocument();
};

it('should render message box text', () => {
  const result = render(<MessageBox text={MESSAGE_TEXT} />);
  assertResult(result);
});

it('should render message box text with custom class name', () => {
  const result = render(<MessageBox text={MESSAGE_TEXT} className={CUSTOM_CLASS_NAME} />);
  assertResult(result, CUSTOM_CLASS_NAME);
});
