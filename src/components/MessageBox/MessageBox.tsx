import React, { FC } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './MessageBox.module.scss';

export interface MessageBoxProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Message box text.
   */
  text: string;
}

/**
 * Displays a message box.
 */
export const MessageBox: FC<MessageBoxProps> = ({
  id,
  style,
  className,
  text
}: MessageBoxProps) => (
  <div id={id} style={style} className={cx(classes.MessageBox, className)}>
    <span>{text}</span>
  </div>
);

export default MessageBox;
