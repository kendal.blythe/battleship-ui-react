import React, { FC } from 'react';
import { select } from '@storybook/addon-knobs';
import { Spinner } from '..';

export default {
  title: 'Spinner',
  component: Spinner
};

export const SpinnerWithDefaultTooltip: FC = () => {
  return <Spinner />;
};

export const SpinnerWithCustomTooltip: FC = () => {
  const Wrapper: FC = () => {
    return <Spinner tooltip="Processing..." />;
  };
  return <Wrapper />;
};

export const SpinnerWithVisibilityDelay: FC = () => {
  const options = {
    '250 milliseconds': 250,
    '1 second': 1000,
    '2 seconds': 2000
  };
  const delay = select('Delay:', options, 250);
  return <Spinner visibilityDelay={delay} />;
};
