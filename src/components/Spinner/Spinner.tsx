import React, { FC, Fragment, MouseEvent, useEffect, useState } from 'react';
import CSS from 'csstype';
import { useTranslation } from 'react-i18next';
import { Backdrop } from '..';
import cx from 'classnames';
import spinner from './spinner.gif';
import classes from './Spinner.module.scss';

export interface SpinnerProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Component tooltip.
   */
  tooltip?: string;
  /**
   * Delay (milliseconds) before the component becomes visible.
   */
  visibilityDelay?: number;
  /**
   * Click event handler.
   */
  onClick?: (event: MouseEvent<HTMLElement>) => void;
}

/**
 * Displays a spinner over a backdrop indicating that the application is busy loading or processing.
 */
export const Spinner: FC<SpinnerProps> = ({
  id,
  style,
  className,
  tooltip,
  visibilityDelay = 0,
  onClick
}: SpinnerProps) => {
  const { t } = useTranslation();
  const tip = tooltip || t('Spinner.loadingTooltip');

  const [hidden, setHidden] = useState(visibilityDelay > 0);

  useEffect(() => {
    let isSubscribed = true;

    if (visibilityDelay > 0) setHidden(true);

    setTimeout(() => {
      isSubscribed && setHidden(false);
    }, visibilityDelay);

    return (): void => {
      isSubscribed = false;
    };
  }, [visibilityDelay]);

  return (
    <Fragment>
      <img
        id={id}
        style={style}
        className={cx(classes.Spinner, className, hidden && classes.hidden)}
        alt={tip}
        title={tip}
        src={spinner}
        onClick={(event: MouseEvent<HTMLElement>): void => onClick?.(event)}
      />
      <Backdrop
        visible={!hidden}
        onClick={(event: MouseEvent<HTMLElement>): void => onClick?.(event)}
      />
    </Fragment>
  );
};

export default Spinner;
