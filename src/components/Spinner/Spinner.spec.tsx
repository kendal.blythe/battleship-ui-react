import React from 'react';
import { render, RenderResult, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { I18nextProvider } from 'react-i18next';
import i18n from '../../i18n/i18nForTest';
import { Spinner } from '..';
import cx from 'classnames';

const DEFAULT_TOOLTIP = i18n.t('Spinner.loadingTooltip');
const CUSTOM_TOOLTIP = 'Processing...';
const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, expectedTooltip: string, className?: string): void => {
  const expectedClassNames = cx('Spinner', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/img/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
  expect(component).toHaveAttribute('title', expectedTooltip);
  expect(component).toHaveAttribute('src', 'spinner.gif');
};

it('should render spinner with default tooltip', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <Spinner />
    </I18nextProvider>
  );
  assertResult(result, DEFAULT_TOOLTIP);
});

it('should render spinner with custom tooltip', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <Spinner tooltip={CUSTOM_TOOLTIP} />
    </I18nextProvider>
  );
  assertResult(result, CUSTOM_TOOLTIP);
});

it('should render spinner with visibility delay', async () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <Spinner visibilityDelay={100} />
    </I18nextProvider>
  );
  assertResult(result, DEFAULT_TOOLTIP, 'hidden');
  const component = result.container.firstChild as Element;
  await waitFor(() => expect(component.className).toBe('Spinner'));
});

it('should render spinner with custom class name', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <Spinner className={CUSTOM_CLASS_NAME} />
    </I18nextProvider>
  );
  assertResult(result, DEFAULT_TOOLTIP, CUSTOM_CLASS_NAME);
});

it('should call click handler on click', () => {
  const onClick = jest.fn(() => undefined);

  const result = render(
    <I18nextProvider i18n={i18n}>
      <Spinner onClick={onClick} />
    </I18nextProvider>
  );
  assertResult(result, DEFAULT_TOOLTIP);

  expect(result.container.childElementCount).toBe(2);
  const img = result.container.firstChild as Element;
  userEvent.click(img);
  const backdrop = result.container.lastChild as Element;
  userEvent.click(backdrop);
  expect(onClick).toHaveBeenCalledTimes(2);
});
