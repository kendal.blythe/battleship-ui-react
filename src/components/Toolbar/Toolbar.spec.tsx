import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { Toolbar } from '..';
import cx from 'classnames';

const CUSTOM_CLASS_NAME = 'test';

const assertResult = (
  result: RenderResult,
  expectedChildTags: Array<string>,
  className?: string
): void => {
  const expectedClassNames = cx('Toolbar', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));

  expect(component.children).toHaveLength(expectedChildTags.length);
  for (let i = 0; i < component.children.length; i++) {
    const elem = component.children[i];
    expect(elem.nodeName).toMatch(new RegExp(expectedChildTags[i], 'i'));
  }
};

it('should render empty toolbar', () => {
  const result = render(<Toolbar />);
  assertResult(result, []);
});

it('should render empty toolbar with custom class name', () => {
  const result = render(<Toolbar className={CUSTOM_CLASS_NAME} />);
  assertResult(result, [], CUSTOM_CLASS_NAME);
});

it('should render toolbar with child elements', () => {
  const result = render(
    <Toolbar>
      <div />
      <span />
      <img />
      <button />
    </Toolbar>
  );
  assertResult(result, ['div', 'span', 'img', 'button']);
});
