import React, { FC, ReactElement } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './Toolbar.module.scss';

export interface ToolbarProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Child elements.
   */
  children?: ReactElement | Array<ReactElement>;
}

/**
 * Displays a toolbar containing child elements laid out horizontally with spacing.
 */
export const Toolbar: FC<ToolbarProps> = ({ id, style, className, children }: ToolbarProps) => (
  <div id={id} style={style} className={cx(classes.Toolbar, className)}>
    {children}
  </div>
);

export default Toolbar;
