/// <reference types="react-scripts" />
import React, { FC } from 'react';
import { linkTo } from '@storybook/addon-links';
import { AppHeading, Button, ButtonDesign, Select, Toolbar, ToolbarSpacer } from '..';
import logo from '../../assets/images/logo.svg';

export default {
  title: 'Toolbar',
  component: Toolbar,
  parameters: {
    a11y: {
      options: {
        rules: {
          'color-contrast': { enabled: false }
        }
      }
    }
  }
};

export const ToolbarWithChildComponents: FC = () => {
  const selectOptions = [{ label: 'Small' }, { label: 'Medium' }, { label: 'Large' }];

  const imgStyle = {
    width: 32,
    height: 32
  };

  return (
    <Toolbar>
      <AppHeading text="Battleship" />
      <Select id="toolbar-select" ariaLabel="Select option" options={selectOptions} />
      <ToolbarSpacer />
      <img alt="" src={logo} style={imgStyle} title="React" />
      <span>React</span>
      <ToolbarSpacer />
      <Button
        text="Link Primary Button"
        design={ButtonDesign.Primary}
        onClick={linkTo('Button', 'Button With Primary Design')}
      />
      <Button
        text="Focus Select"
        onClick={(): void => document.getElementById('toolbar-select').focus()}
      />
    </Toolbar>
  );
};
