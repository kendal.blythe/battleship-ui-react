import React, { FC } from 'react';
import { Ship } from '../../../graphql/battleship-api-types';
import classes from './PlacedShip.module.scss';

export interface PlacedShipProps {
  /**
   * Ship model object.
   */
  ship: Ship;
  /**
   * Ocean grid table cell size (pixels).
   */
  cellSize: number;
}

/**
 * Ocean grid placed ship.
 */
export const PlacedShip: FC<PlacedShipProps> = ({ ship, cellSize }: PlacedShipProps) => {
  if (!ship.coordinates) return null;
  const horizontal = ship.length === 1 || ship.coordinates[0].y === ship.coordinates[1].y;
  const marginSize = 1;
  const shipSize = cellSize * ship.length - marginSize * 2;
  const top = cellSize * ship.coordinates[0].y + marginSize;
  const left = cellSize * ship.coordinates[0].x + marginSize;
  const divStyle = {
    top,
    left,
    width: horizontal ? shipSize : cellSize - marginSize * 2,
    height: horizontal ? cellSize - marginSize * 2 : shipSize
  };
  return <div className={classes.PlacedShip} style={divStyle} title={ship.name} />;
};

export default PlacedShip;
