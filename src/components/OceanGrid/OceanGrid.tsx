import React, { FC, ReactElement, RefObject } from 'react';
import CSS from 'csstype';
import { useTranslation } from 'react-i18next';
import { BombedCoordinate, Grid } from '../../graphql/battleship-api-types';
import PlacedShip from './PlacedShip/PlacedShip';
import TableCell from './TableCell/TableCell';
import cx from 'classnames';
import classes from './OceanGrid.module.scss';

export enum OceanGridDisplaySize {
  Small = 'small',
  Medium = 'medium',
  Large = 'large'
}

export interface OceanGridProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * HTML table element reference object.
   */
  tableRef?: RefObject<HTMLTableElement>;
  /**
   * Ocean grid model object.
   */
  grid: Grid;
  /**
   * Ocean grid display size.
   */
  displaySize?: OceanGridDisplaySize;
  /**
   * Drop bomb event handler.
   */
  onDropBomb?: (x: number, y: number) => void;
}

/**
 * Displays an ocean grid for the Battleship game.
 */
export const OceanGrid: FC<OceanGridProps> = ({
  id,
  style,
  className,
  tableRef,
  grid,
  displaySize = OceanGridDisplaySize.Medium,
  onDropBomb
}: OceanGridProps) => {
  const { t } = useTranslation();

  // determine cell size based on display size
  let cellSize: number;
  switch (displaySize) {
    case OceanGridDisplaySize.Small:
      cellSize = 32; // 2rem
      break;
    case OceanGridDisplaySize.Large:
      cellSize = 48; // 3rem
      break;
    case OceanGridDisplaySize.Medium:
      cellSize = 40; // 2.5rem
      break;
  }

  // utility functions
  const getRowLabel = (rowIndex: number): string => rowIndex.toString();

  const getColumnLabel = (columnIndex: number): string =>
    String.fromCharCode(t('OceanGrid.firstColumnLetter').charCodeAt(0) + columnIndex - 1);

  const getCoordinateLabel = (rowIndex: number, columnIndex: number): string | null => {
    return rowIndex && columnIndex ? getColumnLabel(columnIndex) + getRowLabel(rowIndex) : null;
  };

  const getCoordinateKey = (rowIndex: number, columnIndex: number): string =>
    `${rowIndex}_${columnIndex}`;

  // initialize ship coordinate/label map
  const shipCoordinateLabelMap = new Map<string | null, string>();
  grid.ships?.forEach(ship => {
    ship.coordinates?.forEach(coordinate =>
      shipCoordinateLabelMap.set(getCoordinateLabel(coordinate.y, coordinate.x), ship.name)
    );
  });

  // initialize bombed coordinate map
  const bombedCoordinateMap = new Map<string | null, BombedCoordinate>();
  grid.bombedCoordinates?.forEach(coordinate =>
    bombedCoordinateMap.set(getCoordinateLabel(coordinate.y, coordinate.x), coordinate)
  );

  const createColumns = (rowIndex: number): Array<ReactElement> => {
    const columns = new Array<ReactElement>();
    for (let columnIndex = 0; columnIndex <= grid.size.x; columnIndex += 1) {
      const coordinateLabel = getCoordinateLabel(rowIndex, columnIndex);
      columns.push(
        <TableCell
          key={getCoordinateKey(rowIndex, columnIndex)}
          displaySize={displaySize}
          rowIndex={rowIndex}
          columnIndex={columnIndex}
          rowLabel={getRowLabel(rowIndex)}
          columnLabel={getColumnLabel(columnIndex)}
          bombedCoordinate={bombedCoordinateMap.get(coordinateLabel)}
          shipName={shipCoordinateLabelMap.get(coordinateLabel)}
          onDropBomb={onDropBomb}
        />
      );
    }
    return columns;
  };

  const createRows = (): Array<ReactElement> => {
    const rows = new Array<ReactElement>();
    for (let rowIndex = 0; rowIndex <= grid.size.y; rowIndex += 1) {
      rows.push(<tr key={rowIndex}>{createColumns(rowIndex)}</tr>);
    }
    return rows;
  };

  return (
    <div id={id} style={style} className={cx(classes.OceanGrid, classes[displaySize], className)}>
      <table ref={tableRef}>
        <tbody>{createRows()}</tbody>
      </table>
      {grid.ships?.map((ship, idx) => (
        <PlacedShip key={idx} ship={ship} cellSize={cellSize} />
      ))}
    </div>
  );
};

export default OceanGrid;
