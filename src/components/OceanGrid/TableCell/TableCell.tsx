import React, { FC, KeyboardEvent } from 'react';
import { BombedCoordinate } from '../../../graphql/battleship-api-types';
import classes from './TableCell.module.scss';
import cx from 'classnames';

export interface TableCellProps {
  /**
   * Ocean grid display size.
   */
  displaySize: string;
  /**
   * Coordinate row index.
   */
  rowIndex: number;
  /**
   * Coordinate column index.
   */
  columnIndex: number;
  /**
   * Coordinate row label.
   */
  rowLabel: string;
  /**
   * Coordinate column label.
   */
  columnLabel: string;
  /**
   * Bombed coordinate model object.
   */
  bombedCoordinate?: BombedCoordinate;
  /**
   * Ship name.
   */
  shipName?: string;
  /**
   * Drop bomb event handler.
   */
  onDropBomb?: (x: number, y: number) => void;
}

/**
 * Ocean grid table cell.
 */
export const TableCell: FC<TableCellProps> = ({
  displaySize,
  rowIndex,
  columnIndex,
  rowLabel,
  columnLabel,
  bombedCoordinate,
  shipName,
  onDropBomb
}: TableCellProps) => {
  const cellClassNames = cx(classes.TableCell, classes[displaySize]);
  const headerClassNames = cx(cellClassNames, classes.headerCell);

  const onKeyDown = (event: KeyboardEvent): void => {
    const getRowButtons = (): Array<HTMLButtonElement> =>
      Array.from(document.querySelectorAll(`button[data-row="${rowIndex}"]`));

    const getColumnButtons = (): Array<HTMLButtonElement> =>
      Array.from(document.querySelectorAll(`button[data-col="${columnIndex}"]`));

    const getButtonArrayIndex = (buttons: Array<Element>): number => {
      return buttons.findIndex(button => {
        const buttonRowIdx = button.getAttribute('data-row');
        const buttonColIdx = button.getAttribute('data-col');
        return buttonRowIdx === rowIndex.toString() && buttonColIdx === columnIndex.toString();
      });
    };

    const focusButton = (button: HTMLButtonElement): void => {
      button.focus();
      event.stopPropagation();
      event.preventDefault();
    };

    const focusPreviousButton = (buttons: Array<HTMLButtonElement>): void => {
      let index = getButtonArrayIndex(buttons);
      if (index > 0) {
        focusButton(buttons[--index]!);
      }
    };

    const focusNextButton = (buttons: Array<HTMLButtonElement>): void => {
      let index = getButtonArrayIndex(buttons);
      if (index < buttons.length - 1) {
        focusButton(buttons[++index]!);
      }
    };

    switch (event.key) {
      case 'ArrowLeft':
        focusPreviousButton(getRowButtons());
        break;
      case 'ArrowUp':
        focusPreviousButton(getColumnButtons());
        break;
      case 'ArrowRight':
        focusNextButton(getRowButtons());
        break;
      case 'ArrowDown':
        focusNextButton(getColumnButtons());
        break;
    }
  };

  const coordinateLabel = rowIndex && columnIndex ? columnLabel + rowLabel : null;
  if (coordinateLabel) {
    const tooltip = shipName || coordinateLabel;

    // coordinate cell
    if (bombedCoordinate) {
      // bombed coordinate cell
      const bombedForwardSlashClassName = bombedCoordinate.hit
        ? classes.hitForwardSlash
        : classes.missForwardSlash;
      const bombedBackslashClassName = bombedCoordinate.hit
        ? classes.hitBackslash
        : classes.missBackslash;
      return (
        <td className={cellClassNames} title={tooltip} data-testid={coordinateLabel}>
          <div className={bombedForwardSlashClassName} />
          <div className={bombedBackslashClassName} />
        </td>
      );
    } else if (onDropBomb) {
      // bombable coordinate cell
      return (
        <td className={cellClassNames} title={tooltip} data-testid={coordinateLabel}>
          <button
            type="button"
            aria-label={coordinateLabel}
            data-testid={`button${coordinateLabel}`}
            data-row={rowIndex}
            data-col={columnIndex}
            onClick={(): void => onDropBomb(columnIndex, rowIndex)}
            onKeyDown={onKeyDown}
          >
            {' '}
          </button>
        </td>
      );
    } else {
      // non-bombable coordinate cell
      return <td className={cellClassNames} title={tooltip} data-testid={coordinateLabel} />;
    }
  } else if (rowIndex) {
    // header row cell
    return (
      <td className={headerClassNames}>
        <span>{rowLabel}</span>
      </td>
    );
  } else if (columnIndex) {
    // header column cell
    return (
      <td className={headerClassNames}>
        <span>{columnLabel}</span>
      </td>
    );
  } else {
    // empty header cell
    return <td className={headerClassNames} />;
  }
};

export default TableCell;
