import React, { FC } from 'react';
import { select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { OceanGrid, OceanGridDisplaySize } from '..';
import { Grid } from '../../graphql/battleship-api-types';

export default {
  title: 'OceanGrid',
  component: OceanGrid
};

const createTestGrid = (sizeX: number, sizeY: number, dropBombs = false): Grid => {
  const grid = {
    size: {
      x: sizeX,
      y: sizeY
    },
    ships: [
      {
        name: 'Battleship',
        length: 3,
        coordinates: [
          {
            x: 2,
            y: 2
          },
          {
            x: 3,
            y: 2
          },
          {
            x: 4,
            y: 2
          }
        ]
      },
      {
        name: 'Submarine',
        length: 2,
        coordinates: [
          {
            x: 3,
            y: 3
          },
          {
            x: 3,
            y: 4
          }
        ]
      }
    ]
  } as Grid;
  if (dropBombs) {
    grid.bombedCoordinates = [
      {
        x: 1,
        y: 1,
        hit: false
      },
      {
        x: 3,
        y: 3,
        hit: true
      },
      {
        x: 5,
        y: 5,
        hit: false
      }
    ];
  }
  return grid;
};

export const OceanGridWithBombs: FC = () => {
  const gridSizeOptions = {
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 10,
    '11': 11,
    '12': 12,
    '13': 13,
    '14': 14,
    '15': 15,
    '16': 16,
    '17': 17,
    '18': 18,
    '19': 19,
    '20': 20,
    '21': 21,
    '22': 22,
    '23': 23,
    '24': 24,
    '25': 25,
    '26': 26
  };

  const displaySizeOptions = {
    Small: OceanGridDisplaySize.Small,
    Medium: OceanGridDisplaySize.Medium,
    Large: OceanGridDisplaySize.Large
  };

  const rows = select('Rows:', gridSizeOptions, 6);
  const columns = select('Columns:', gridSizeOptions, 8);
  const displaySize = select('Display size:', displaySizeOptions, OceanGridDisplaySize.Medium);
  const grid = createTestGrid(columns, rows, true);

  return <OceanGrid grid={grid} displaySize={displaySize} onDropBomb={action('dropBomb')} />;
};

export const OceanGridWithSmallDisplaySize: FC = () => {
  const grid = createTestGrid(5, 5);
  return <OceanGrid grid={grid} displaySize={OceanGridDisplaySize.Small} />;
};

export const OceanGridWithMediumDisplaySize: FC = () => {
  const grid = createTestGrid(5, 5);
  return <OceanGrid grid={grid} displaySize={OceanGridDisplaySize.Medium} />;
};

export const OceanGridWithLargeDisplaySize: FC = () => {
  const grid = createTestGrid(5, 5);
  return <OceanGrid grid={grid} displaySize={OceanGridDisplaySize.Large} />;
};
