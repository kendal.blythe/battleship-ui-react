import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { I18nextProvider } from 'react-i18next';
import i18n from '../../i18n/i18nForTest';
import { OceanGrid } from '..';
import {
  OceanGridWithBombs,
  OceanGridWithSmallDisplaySize,
  OceanGridWithMediumDisplaySize,
  OceanGridWithLargeDisplaySize
} from './OceanGrid.stories';
import { Grid } from '../../graphql/battleship-api-types';
import cx from 'classnames';

const CUSTOM_CLASS_NAME = 'test';

const assertResult = (result: RenderResult, className?: string): void => {
  const expectedClassNames = cx('OceanGrid', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/div/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));
};

const assertOceanGridCells = (sizeX: number, sizeY: number): void => {
  for (let x = 1; x <= sizeX; x++) {
    for (let y = 1; y <= sizeY; y++) {
      const ch = String.fromCharCode('A'.charCodeAt(0) + x - 1);
      const cellTitle = ch + y;
      const cell = screen.queryByTestId(cellTitle);
      expect(cell).toBeInTheDocument();
    }
  }
};

const getButton = (container: Element, x: number, y: number): HTMLButtonElement => {
  const button = container.querySelector(`button[data-col="${x}"][data-row="${y}"]`);
  expect(button).toBeInTheDocument();
  return button as HTMLButtonElement;
};

it('should render ocean grid with bombs', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGridWithBombs />
    </I18nextProvider>
  );
  assertResult(result, 'medium');
  assertOceanGridCells(8, 6);
});

it('should render ocean grid with small display size', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGridWithSmallDisplaySize />
    </I18nextProvider>
  );
  assertResult(result, 'small');
  assertOceanGridCells(5, 5);
});

it('should render ocean grid with medium display size', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGridWithMediumDisplaySize />
    </I18nextProvider>
  );
  assertResult(result, 'medium');
  assertOceanGridCells(5, 5);
});

it('should render ocean grid with large display size', () => {
  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGridWithLargeDisplaySize />
    </I18nextProvider>
  );
  assertResult(result, 'large');
  assertOceanGridCells(5, 5);
});

it('should call bomb drop handler on coordinate click', () => {
  let bombDroppedC3 = false;
  const onDropBomb = jest.fn((x: number, y: number): void => {
    if (x === 3 && y === 3) bombDroppedC3 = true;
  });

  const grid = {
    size: {
      x: 5,
      y: 5
    },
    ships: [
      {
        name: 'Battleship',
        length: 3
      },
      {
        name: 'Submarine',
        length: 2
      }
    ]
  } as Grid;

  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGrid
        grid={grid}
        className={CUSTOM_CLASS_NAME}
        onDropBomb={(x: number, y: number): void => onDropBomb(x, y)}
      />
    </I18nextProvider>
  );
  assertResult(result, CUSTOM_CLASS_NAME);
  assertOceanGridCells(5, 5);

  const button = screen.queryByTestId('buttonC3');
  expect(button).toBeInTheDocument();
  userEvent.click(button!);
  expect(onDropBomb).toHaveBeenCalledTimes(1);
  expect(bombDroppedC3).toBe(true);
});

it('should change focus on valid arrow key navigation', () => {
  const grid = {
    size: {
      x: 5,
      y: 5
    }
  } as Grid;

  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGrid grid={grid} onDropBomb={(): void => undefined} />
    </I18nextProvider>
  );
  assertResult(result);
  assertOceanGridCells(5, 5);

  const button11 = getButton(result.container, 1, 1);
  const button12 = getButton(result.container, 1, 2);
  const button21 = getButton(result.container, 2, 1);
  const button22 = getButton(result.container, 2, 2);

  fireEvent.keyDown(button11, { key: 'ArrowRight' });
  expect(button21).toHaveFocus();
  fireEvent.keyDown(button21, { key: 'ArrowDown' });
  expect(button22).toHaveFocus();
  fireEvent.keyDown(button22, { key: 'ArrowLeft' });
  expect(button12).toHaveFocus();
  fireEvent.keyDown(button12, { key: 'ArrowUp' });
  expect(button11).toHaveFocus();
});

it('should not change focus on invalid arrow key navigation', () => {
  const grid = {
    size: {
      x: 5,
      y: 5
    }
  } as Grid;

  const result = render(
    <I18nextProvider i18n={i18n}>
      <OceanGrid grid={grid} onDropBomb={(): void => undefined} />
    </I18nextProvider>
  );
  assertResult(result);
  assertOceanGridCells(5, 5);

  const button11 = getButton(result.container, 1, 1);
  const button55 = getButton(result.container, 5, 5);

  button11.focus();

  fireEvent.keyDown(button11, { key: 'ArrowUp' });
  expect(button11).toHaveFocus();
  fireEvent.keyDown(button11, { key: 'ArrowLeft' });
  expect(button11).toHaveFocus();
  fireEvent.keyDown(button55, { key: 'ArrowDown' });
  expect(button11).toHaveFocus();
  fireEvent.keyDown(button55, { key: 'ArrowRight' });
  expect(button11).toHaveFocus();
});
