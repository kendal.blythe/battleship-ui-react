import React, { FC, useState } from 'react';
import { decorate } from '@storybook/addon-actions';
import { Select } from '..';

export default {
  title: 'Select',
  component: Select,
  parameters: {
    a11y: {
      options: {
        rules: {
          'color-contrast': { enabled: false }
        }
      }
    }
  }
};

export const SelectWithOptionLabel: FC = () => {
  const options = [{ label: 'Small' }, { label: 'Medium' }, { label: 'Large' }];
  const [currentValue, setCurrentValue] = useState('Small');

  const actionDecorator = decorate([
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (args): Array<any> => {
      setCurrentValue(args[0].value);
      return args;
    }
  ]);

  return (
    <Select
      ariaLabel="Select option"
      value={currentValue}
      options={options}
      onChange={actionDecorator.action('change')}
    />
  );
};

export const SelectWithOptionLabelAndValue: FC = () => {
  const options = [
    { value: 'S', label: 'Small' },
    { value: 'M', label: 'Medium' },
    { value: 'L', label: 'Large' }
  ];
  const [currentValue, setCurrentValue] = useState('M');

  const actionDecorator = decorate([
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (args): Array<any> => {
      setCurrentValue(args[0].value);
      return args;
    }
  ]);

  return (
    <Select
      ariaLabel="Select option"
      value={currentValue}
      options={options}
      onChange={actionDecorator.action('change')}
    />
  );
};
