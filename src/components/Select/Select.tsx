import React, { FC, ChangeEvent } from 'react';
import CSS from 'csstype';
import cx from 'classnames';
import classes from './Select.module.scss';

export interface SelectOption {
  value?: string | number;
  label: string;
}

export interface SelectChangeEvent {
  value: string | number;
  event: ChangeEvent<HTMLSelectElement>;
}

export interface SelectProps {
  /**
   * Component HTML element id.
   */
  id?: string;
  /**
   * Component HTML element data-testid.
   */
  testId?: string;
  /**
   * Component HTML element ARIA label.
   */
  ariaLabel?: string;
  /**
   * CSS styles added to the component.
   */
  style?: CSS.Properties;
  /**
   * CSS class names added to the component.
   */
  className?: string;
  /**
   * Select component value.
   */
  value?: string | number;
  /**
   * Select component options.
   */
  options?: Array<SelectOption>;
  /**
   * Change event handler.
   */
  onChange?: (event: SelectChangeEvent) => void;
}

/**
 * Displays a drop-down list allowing the user to select a value from a list of options.
 */
export const Select: FC<SelectProps> = ({
  id,
  testId,
  ariaLabel,
  style,
  className,
  value,
  options,
  onChange
}: SelectProps) => (
  <select
    id={id}
    data-testid={testId}
    aria-label={ariaLabel}
    style={style}
    className={cx(classes.Select, className)}
    value={value}
    onChange={(event: ChangeEvent<HTMLSelectElement>): void =>
      onChange?.({ value: event.target.value, event })
    }
  >
    {options?.map(({ value, label }) => (
      <option key={value || label} value={value}>
        {label}
      </option>
    ))}
  </select>
);

export default Select;
