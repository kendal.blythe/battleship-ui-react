import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Select, SelectOption } from '..';
import cx from 'classnames';

const OPTIONS_WITH_LABEL = [{ label: 'Small' }, { label: 'Medium' }, { label: 'Large' }];
const OPTIONS_WITH_LABEL_AND_VALUE = [
  { value: 'S', label: 'Small' },
  { value: 'M', label: 'Medium' },
  { value: 'L', label: 'Large' }
];
const CUSTOM_CLASS_NAME = 'test';

const assertResult = (
  result: RenderResult,
  options: Array<SelectOption>,
  className?: string
): void => {
  const expectedClassNames = cx('Select', className);
  const component = result.container.firstChild as Element;
  expect(component.nodeName).toMatch(/select/i);
  expectedClassNames.split(' ').forEach(className => expect(component).toHaveClass(className));

  const optionElements = component.querySelectorAll('option');
  expect(optionElements).toHaveLength(options.length);
  if (options === OPTIONS_WITH_LABEL) {
    optionElements.forEach((option, i) => {
      const { label } = options[i];
      expect(option.getAttribute('value')).toBeFalsy();
      expect(option).toHaveTextContent(label);
    });
  } else {
    optionElements.forEach((option, i) => {
      const { value, label } = options[i];
      expect(option).toHaveAttribute('value', value);
      expect(option).toHaveTextContent(label);
    });
  }
};

it('should render select with options with label only', () => {
  const result = render(<Select options={OPTIONS_WITH_LABEL} />);
  assertResult(result, OPTIONS_WITH_LABEL);
});

it('should render select with custom class name', () => {
  const result = render(<Select options={OPTIONS_WITH_LABEL} className={CUSTOM_CLASS_NAME} />);
  assertResult(result, OPTIONS_WITH_LABEL, CUSTOM_CLASS_NAME);
});

it('should render select with options with label and value', () => {
  const result = render(<Select options={OPTIONS_WITH_LABEL_AND_VALUE} />);
  assertResult(result, OPTIONS_WITH_LABEL_AND_VALUE);
});

it('should call change handler on change', () => {
  const onChange = jest.fn(() => undefined);

  const result = render(<Select options={OPTIONS_WITH_LABEL} onChange={onChange} />);
  assertResult(result, OPTIONS_WITH_LABEL);

  const select = result.container.firstChild as Element;
  userEvent.selectOptions(select, OPTIONS_WITH_LABEL[1].label);
  expect(onChange).toHaveBeenCalledTimes(1);
});
