import React from 'react';
import { addDecorator, addParameters } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';
import { withKnobs } from '@storybook/addon-knobs';
import { AppSuspense } from '../src/components';
import '../src/i18n/i18n';
import '../src/index.scss';

addParameters({
  docs: {
    container: DocsContainer,
    page: DocsPage
  }
});

addDecorator(withKnobs);
addDecorator(withA11y);
addDecorator(storyFn => <AppSuspense>{storyFn()}</AppSuspense>);
