# Battleship UI (React)

## Introduction

<a href="https://en.wikipedia.org/wiki/Battleship_(game)">Battleship</a> is a classic strategy/guessing game of naval combat. This project implements a web application written in <a href="https://reactjs.org/">React</a> for playing Battleship. See the corresponding <a href="https://gitlab.com/kendal.blythe/battleship-api-java">Battleship API (Java)</a> project for the <a href="https://graphql.org/">GraphQL</a> API.

## Technologies

- <a href="https://reactjs.org/">React</a>
- <a href="https://www.javascript.com/">JavaScript</a>
- <a href="https://www.typescriptlang.org/">TypeScript</a>
- <a href="https://www.apollographql.com/docs/react/">Apollo Client</a>
- <a href="https://graphql-code-generator.com/">GraphQL Codegen</a>
- <a href="https://www.i18next.com/">i18next</a>
- <a href="https://sass-lang.com/">SASS</a>
- <a href="https://csstools.github.io/sanitize.css/">sanitize.css</a>
- <a href="https://storybook.js.org/">Storybook</a>
- <a href="https://jestjs.io/">Jest</a>
- <a href="https://webpack.js.org/">Webpack</a>
- <a href="https://testing-library.com/">React Testing Library</a>
- <a href="https://www.cypress.io/">cypress.io</a>
- <a href="https://code.visualstudio.com/">Visual Studio Code</a>
- <a href="https://eslint.org/">ESLint</a>
- <a href="https://prettier.io/">Prettier</a>
- <a href="https://yarnpkg.com/">Yarn</a>
- <a href="https://gitlab.com/">GitLab</a>

## Play Battleship

You can play a game of Battleship here.

- <a href="https://battleship-ui-react.herokuapp.com/">https://battleship-ui-react.herokuapp.com/</a>

Be patient while the application loads the first time! Both Battleship projects are automatically deployed to <a href="https://www.heroku.com/">Heroku Cloud Platform</a> (free tier) using GitLab CI/CD.

## Getting Started

After you clone the Git repository, you can run the following <a href="https://yarnpkg.com/">Yarn</a> commands.

- `yarn` - install packages
- `yarn build` - build production app
- `yarn build:lib` - build production component library bundle
- `yarn start` - run development app
- `yarn start:prod` - run production app
- `yarn test` - run unit tests in interactive mode
- `yarn test:all` - run unit tests non-interactively with code coverage
- `yarn test:e2e` - Run unit and end-to-end tests and generate combined code coverage
- `yarn e2e` - run end-to-end tests in headless browser
- `yarn e2e:chrome` - run end-to-end tests in Chrome browser
- `yarn e2e:edge` - run end-to-end tests in Edge browser
- `yarn e2e:firefox` - run end-to-end tests in Firefox browser
- `yarn e2e:ci` - start development app and run end-to-end tests in headless mode
- `yarn storybook` - run Storybook
- `yarn graphql-codegen` - generate TypeScript definitions from GraphQL schema

## Testing

One of the goals for this project was to achieve 100% code coverage. Here are the results!

```
$ node ./scripts/mergeCoverage.js
coverage files in coverage/reports merged into coverage.json
---------------------------------|----------|----------|----------|----------|-----------|
File                             |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered |
---------------------------------|----------|----------|----------|----------|-----------|
All files                        |      100 |      100 |      100 |      100 |           |
 components/AppHeading           |      100 |      100 |      100 |      100 |           |
  AppHeading.tsx                 |      100 |      100 |      100 |      100 |           |
 components/AppSuspense          |      100 |      100 |      100 |      100 |           |
  AppSuspense.tsx                |      100 |      100 |      100 |      100 |           |
 components/Backdrop             |      100 |      100 |      100 |      100 |           |
  Backdrop.tsx                   |      100 |      100 |      100 |      100 |           |
 components/Button               |      100 |      100 |      100 |      100 |           |
  Button.tsx                     |      100 |      100 |      100 |      100 |           |
 components/MessageBox           |      100 |      100 |      100 |      100 |           |
  MessageBox.tsx                 |      100 |      100 |      100 |      100 |           |
 components/OceanGrid            |      100 |      100 |      100 |      100 |           |
  OceanGrid.tsx                  |      100 |      100 |      100 |      100 |           |
 components/OceanGrid/PlacedShip |      100 |      100 |      100 |      100 |           |
  PlacedShip.tsx                 |      100 |      100 |      100 |      100 |           |
 components/OceanGrid/TableCell  |      100 |      100 |      100 |      100 |           |
  TableCell.tsx                  |      100 |      100 |      100 |      100 |           |
 components/Select               |      100 |      100 |      100 |      100 |           |
  Select.tsx                     |      100 |      100 |      100 |      100 |           |
 components/Spinner              |      100 |      100 |      100 |      100 |           |
  Spinner.tsx                    |      100 |      100 |      100 |      100 |           |
 components/Toolbar              |      100 |      100 |      100 |      100 |           |
  Toolbar.tsx                    |      100 |      100 |      100 |      100 |           |
 components/ToolbarSpacer        |      100 |      100 |      100 |      100 |           |
  ToolbarSpacer.tsx              |      100 |      100 |      100 |      100 |           |
 containers                      |      100 |      100 |      100 |      100 |           |
  App.tsx                        |      100 |      100 |      100 |      100 |           |
 containers/ConfigurationPage    |      100 |      100 |      100 |      100 |           |
  ConfigurationPage.tsx          |      100 |      100 |      100 |      100 |           |
 containers/GamePage             |      100 |      100 |      100 |      100 |           |
  GamePage.tsx                   |      100 |      100 |      100 |      100 |           |
 graphql                         |      100 |      100 |      100 |      100 |           |
  fragments.ts                   |      100 |      100 |      100 |      100 |           |
  index.ts                       |      100 |      100 |      100 |      100 |           |
  mapper.ts                      |      100 |      100 |      100 |      100 |           |
  mutations.ts                   |      100 |      100 |      100 |      100 |           |
  queries.ts                     |      100 |      100 |      100 |      100 |           |
---------------------------------|----------|----------|----------|----------|-----------|

Test Suites: 17 passed, 17 total
Unit Tests:  68 passed, 68 total
E2E Tests:    1 passed,  1 total
```

:smile:

## Developer

<a href="https://www.linkedin.com/in/kendal-blythe/">Kendal Blythe</a> - Check me out on LinkedIn!
